import os
import setuptools
from setuptools import setup, find_packages

NAME = 'ika'

with open("README.md", "r") as fh:
    long_description = fh.read()

# Load the package's __version__.py module as a dictionary.
here = os.path.abspath(os.path.dirname(__file__))
about = {}
with open(os.path.join(here, NAME, '__version__.py')) as f:
    exec(f.read(), about)


setup(
    name=NAME,
    version=about['__version__'],
    author='Veronica Lai',
    description='Unofficial Python package to control IKA products; not affiliated with IKA.',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://gitlab.com/heingroup/ika',
    packages=setuptools.find_packages(),
     classifiers=[
        "Programming Language :: Python :: 3",
    ],
    python_requires='>=3.6',
    install_requires=[
        "ftdi_serial",
        "numpy",
        'hein_utilities==3.14.2',
        'hein_control==7.4.7',
    ],
)
